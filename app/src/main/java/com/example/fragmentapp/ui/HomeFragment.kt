package com.example.fragmentapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.fragmentapp.R
import com.example.fragmentapp.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGoToRed.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_redFragment2)
        }
        binding.btnGoToGreen.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_greenFragment)
        }
        binding.btnGoToGrey.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_greyFragment)
        }
        binding.btnGoToYellow.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_yellowFragment)
        }
        binding.btnGoToPurple.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_purpleFragment)
        }
        binding.btnGoToBlue.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_blueFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}