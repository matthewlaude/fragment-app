package com.example.fragmentapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.fragmentapp.R
import com.example.fragmentapp.databinding.FragmentPurpleBinding
import com.example.fragmentapp.databinding.FragmentRedBinding

class PurpleFragment : Fragment(R.layout.fragment_purple) {

    private var _binding: FragmentPurpleBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentPurpleBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}